﻿namespace TariffTrade.Backend.Models
{
	using Domain.Models;

	public class LocalDataContext : DataContext
	{
		public System.Data.Entity.DbSet<TariffTrade.Common.Models.Codes> Codes { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Categories> Categories { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Informations> Informations { get; set; }
    }
}