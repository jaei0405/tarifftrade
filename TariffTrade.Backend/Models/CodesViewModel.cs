﻿namespace TariffTrade.Backend.Models
{
    using Common.Models;
    using System.Web.Mvc;

    public class CodesViewModel : Codes
    {
        public int SelectCountryID { get; set; }
        public SelectList CountriesSelectList { get; set; }
        public string Hierarchy { get; set; }
        public string Description { get; set; }

    }
}