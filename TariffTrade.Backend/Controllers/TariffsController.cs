﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TariffTrade.Backend.Models;
using TariffTrade.Common.Models;

namespace TariffTrade.Backend.Controllers
{
    public class TariffsController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Tariffs
        public async Task<ActionResult> Index()
        {
            return View(await db.Tariffs.ToListAsync());
        }

        // GET: Tariffs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tariffs tariffs = await db.Tariffs.FindAsync(id);
            if (tariffs == null)
            {
                return HttpNotFound();
            }
            return View(tariffs);
        }

        // GET: Tariffs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tariffs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TariffId,Year,Tariff,CreateOn,UpdateOn")] Tariffs tariffs)
        {
            if (ModelState.IsValid)
            {
                db.Tariffs.Add(tariffs);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tariffs);
        }

        // GET: Tariffs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tariffs tariffs = await db.Tariffs.FindAsync(id);
            if (tariffs == null)
            {
                return HttpNotFound();
            }
            return View(tariffs);
        }

        // POST: Tariffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TariffId,Year,Tariff,CreateOn,UpdateOn")] Tariffs tariffs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tariffs).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tariffs);
        }

        // GET: Tariffs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tariffs tariffs = await db.Tariffs.FindAsync(id);
            if (tariffs == null)
            {
                return HttpNotFound();
            }
            return View(tariffs);
        }

        // POST: Tariffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Tariffs tariffs = await db.Tariffs.FindAsync(id);
            db.Tariffs.Remove(tariffs);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
