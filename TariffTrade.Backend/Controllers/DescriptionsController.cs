﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TariffTrade.Backend.Models;
using TariffTrade.Common.Models;

namespace TariffTrade.Backend.Controllers
{
    public class DescriptionsController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Descriptions
        public async Task<ActionResult> Index()
        {
            var descriptions = db.Descriptions.Include(d => d.Countries);
            return View(await descriptions.ToListAsync());
        }

        // GET: Descriptions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Descriptions descriptions = await db.Descriptions.FindAsync(id);
            if (descriptions == null)
            {
                return HttpNotFound();
            }
            return View(descriptions);
        }

        // GET: Descriptions/Create
        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Country");
            return View();
        }

        // POST: Descriptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DescriptionId,Hierarchy,Description,CreateOn,UpdateOn,CountryId")] Descriptions descriptions)
        {
            if (ModelState.IsValid)
            {
                db.Descriptions.Add(descriptions);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Country", descriptions.CountryId);
            return View(descriptions);
        }

        // GET: Descriptions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Descriptions descriptions = await db.Descriptions.FindAsync(id);
            if (descriptions == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Country", descriptions.CountryId);
            return View(descriptions);
        }

        // POST: Descriptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DescriptionId,Hierarchy,Description,CreateOn,UpdateOn,CountryId")] Descriptions descriptions)
        {
            if (ModelState.IsValid)
            {
                db.Entry(descriptions).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Country", descriptions.CountryId);
            return View(descriptions);
        }

        // GET: Descriptions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Descriptions descriptions = await db.Descriptions.FindAsync(id);
            if (descriptions == null)
            {
                return HttpNotFound();
            }
            return View(descriptions);
        }

        // POST: Descriptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Descriptions descriptions = await db.Descriptions.FindAsync(id);
            db.Descriptions.Remove(descriptions);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
