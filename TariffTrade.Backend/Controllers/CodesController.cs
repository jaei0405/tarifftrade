﻿namespace TariffTrade.Backend.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Backend.Models;
    using Common.Models;

    public class CodesController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Codes
        public async Task<ActionResult> Index()
        {
            var a = await db.Codes.ToListAsync();
            return View(a);
        }

        // GET: Codes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Codes codes = await db.Codes.FindAsync(id);
            if (codes == null)
            {
                return HttpNotFound();
            }
            return View(codes);
        }

        // GET: Codes/Create
        public ActionResult Create()
        {
            var codes = new CodesViewModel();
            codes.CountriesSelectList = new SelectList(db.Countries, "CountryId", "Country", 1);
            return View(codes);
        }
        // POST: Codes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CodesViewModel codes)
        {
            if (ModelState.IsValid)
            {
                var code = ToCode(codes);
                var description = ToDescription(codes);
                db.Codes.Add(code);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(codes);
        }

        private Descriptions ToDescription(CodesViewModel codes)
        {
            return new Descriptions
            {
                Description = codes.Description,
                Hierarchy = codes.Hierarchy,
                CountryId = codes.SelectCountryID,
                CreateOn = codes.CreateOn,
                UpdateOn = codes.UpdateOn,
            };
        }

        private Codes ToCode(CodesViewModel codes)
        {
            var countries = new List<Countries>();
            countries.Add(db.Countries.Find(codes.SelectCountryID));
            var description = new List<Descriptions>();
            description.Add(ToDescription(codes));
            return new Codes
            {
                Code = codes.Code,
                Level = codes.Level,
                Year = codes.Year,
                CreateOn = codes.CreateOn,
                UpdateOn = codes.UpdateOn,
                Countries = countries,
                Descriptions = description,
            };
        }

        // GET: Codes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Codes codes = await db.Codes.FindAsync(id);
            if (codes == null)
            {
                return HttpNotFound();
            }
            return View(codes);
        }

        // POST: Codes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CodeId,Code,Level,Year,CreateOn,UpdateOn")] Codes codes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(codes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(codes);
        }

        // GET: Codes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Codes codes = await db.Codes.FindAsync(id);
            if (codes == null)
            {
                return HttpNotFound();
            }
            return View(codes);
        }

        // POST: Codes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Codes codes = await db.Codes.FindAsync(id);
            db.Codes.Remove(codes);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
