﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TariffTrade.Backend.Models;
using TariffTrade.Common.Models;

namespace TariffTrade.Backend.Controllers
{
    public class InformationsController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Informations
        public async Task<ActionResult> Index()
        {
            return View(await db.Informations.ToListAsync());
        }

        // GET: Informations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Informations informations = await db.Informations.FindAsync(id);
            if (informations == null)
            {
                return HttpNotFound();
            }
            return View(informations);
        }

        // GET: Informations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Informations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "InformationId,TariffBase,Iva,CreateOn,UpdateOn")] Informations informations)
        {
            if (ModelState.IsValid)
            {
                db.Informations.Add(informations);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(informations);
        }

        // GET: Informations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Informations informations = await db.Informations.FindAsync(id);
            if (informations == null)
            {
                return HttpNotFound();
            }
            return View(informations);
        }

        // POST: Informations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "InformationId,TariffBase,Iva,CreateOn,UpdateOn")] Informations informations)
        {
            if (ModelState.IsValid)
            {
                db.Entry(informations).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(informations);
        }

        // GET: Informations/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Informations informations = await db.Informations.FindAsync(id);
            if (informations == null)
            {
                return HttpNotFound();
            }
            return View(informations);
        }

        // POST: Informations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Informations informations = await db.Informations.FindAsync(id);
            db.Informations.Remove(informations);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
