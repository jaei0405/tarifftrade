﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TariffTrade.Backend.Startup))]
namespace TariffTrade.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
