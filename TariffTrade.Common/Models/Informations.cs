﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Informations
    {
        #region ctor
        public Informations()
        {
            Tariffs = new HashSet<Tariffs>();
        }
        #endregion

        #region Properties
        [Key]
        public int InformationId { get; set; }
        [Required]
        public int TariffBase { get; set; }
        [Required]
        public int Iva { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
        public virtual ICollection<Tariffs> Tariffs { get; set; }
        #endregion
    }
}
