﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Codes
	{
        #region ctor
        public Codes()
        {
            Countries = new HashSet<Countries>();
            Descriptions = new HashSet<Descriptions>();
        }
        #endregion

        [Key]
		public int CodeId { get; set; }
		[Required]
		public string Code { get; set; }
		public int Level { get; set; }
		public DateTime Year { get; set; }
		public DateTime CreateOn { get; set; }
		public DateTime UpdateOn { get; set; }
        public virtual ICollection<Countries> Countries { get; set; }
        public virtual ICollection<Descriptions> Descriptions { get; set; }

    }
}
