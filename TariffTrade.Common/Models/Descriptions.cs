﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Descriptions
    {

        #region ctor
        public Descriptions()
        {
            Codes = new HashSet<Codes>();
        }
        #endregion

        [Key]
        public int DescriptionId { get; set; }
        [Required]
        public string Hierarchy { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
        public int CountryId { get; set; }
        public Countries Countries { get; set; }
        public virtual ICollection<Codes> Codes { get; set; }

    }
}
