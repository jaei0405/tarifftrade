﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [NotMapped]
    public class Products
    {
        public string Hierarchy { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int Level { get; set; }
        public DateTime Year { get; set; }
        public string Country { get; set; }
        public DateTime CovenantDate { get; set; }
    }
}
