﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Users
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public string Rut { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        public string CivilStatus { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string Profetion { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        [Required]
        public string CellPhone { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
    }
}
