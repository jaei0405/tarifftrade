﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Operations
    {
        #region ctor
        public Operations()
        {
            Countries = new HashSet<Countries>();
        }
        #endregion

        #region Properties
        [Key]
        public int OperationId { get; set; }
        [Required]
        public string Operation { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
        public virtual ICollection<Countries> Countries { get; set; }
        #endregion
    }
}
