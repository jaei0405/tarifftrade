﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Categories
    {
        #region ctor
        public Categories()
        {
            Tariffs = new HashSet<Tariffs>();
        }
        #endregion

        #region Properties
        [Key]
        public int CategoryId { get; set; }
        [Required]
        public string Category { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
        public virtual ICollection<Tariffs> Tariffs { get; set; }
        #endregion
    }
}
