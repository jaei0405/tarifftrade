﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Countries
    {

        #region ctor
        public Countries()
        {
            Operations = new HashSet<Operations>();
            Codes = new HashSet<Codes>();
        }
        #endregion

        #region Properties
        [Key]
        public int CountryId { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public DateTime CovenantDate { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
        public ICollection<Descriptions> Descriptions { get; set; }
        public ICollection<Operations> Operations { get; set; }
        public ICollection<Codes> Codes { get; set; }
        #endregion
    }
}
