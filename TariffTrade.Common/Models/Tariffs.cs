﻿namespace TariffTrade.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Tariffs
    {
        #region ctor
        public Tariffs()
        {
            Categories = new HashSet<Categories>();
        }
        #endregion

        #region Properties
        [Key]
        public int TariffId { get; set; }
        [Required]
        public DateTime Year { get; set; }
        [Required]
        public int Tariff { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime UpdateOn { get; set; }
        public virtual ICollection<Categories> Categories { get; set; }
        #endregion
    }
}
