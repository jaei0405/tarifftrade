﻿namespace TariffTrade.Domain.Models
{
	using System.Data.Entity;

	public class DataContext : DbContext
	{
		public DataContext() : base("DefaultConnection")
		{
		}

		public System.Data.Entity.DbSet<TariffTrade.Common.Models.Codes> Codes { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Categories> Categories { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Descriptions> Descriptions { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Countries> Countries { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Informations> Informations { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Tariffs> Tariffs { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Users> Users { get; set; }

        public System.Data.Entity.DbSet<TariffTrade.Common.Models.Operations> Operations { get; set; }
    }
}
