﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TariffTrade.Common.Models;
using TariffTrade.Domain.Models;

namespace TariffTrade.API.Controllers
{
    public class InformationsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Informations
        public IQueryable<Informations> GetInformations()
        {
            return db.Informations;
        }

        // GET: api/Informations/5
        [ResponseType(typeof(Informations))]
        public async Task<IHttpActionResult> GetInformations(int id)
        {
            Informations informations = await db.Informations.FindAsync(id);
            if (informations == null)
            {
                return NotFound();
            }

            return Ok(informations);
        }

        // PUT: api/Informations/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInformations(int id, Informations informations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != informations.InformationId)
            {
                return BadRequest();
            }

            db.Entry(informations).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InformationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Informations
        [ResponseType(typeof(Informations))]
        public async Task<IHttpActionResult> PostInformations(Informations informations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Informations.Add(informations);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = informations.InformationId }, informations);
        }

        // DELETE: api/Informations/5
        [ResponseType(typeof(Informations))]
        public async Task<IHttpActionResult> DeleteInformations(int id)
        {
            Informations informations = await db.Informations.FindAsync(id);
            if (informations == null)
            {
                return NotFound();
            }

            db.Informations.Remove(informations);
            await db.SaveChangesAsync();

            return Ok(informations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InformationsExists(int id)
        {
            return db.Informations.Count(e => e.InformationId == id) > 0;
        }
    }
}