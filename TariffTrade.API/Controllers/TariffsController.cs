﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TariffTrade.Common.Models;
using TariffTrade.Domain.Models;

namespace TariffTrade.API.Controllers
{
    public class TariffsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Tariffs
        public IQueryable<Tariffs> GetTariffs()
        {
            return db.Tariffs;
        }

        // GET: api/Tariffs/5
        [ResponseType(typeof(Tariffs))]
        public async Task<IHttpActionResult> GetTariffs(int id)
        {
            Tariffs tariffs = await db.Tariffs.FindAsync(id);
            if (tariffs == null)
            {
                return NotFound();
            }

            return Ok(tariffs);
        }

        // PUT: api/Tariffs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTariffs(int id, Tariffs tariffs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tariffs.TariffId)
            {
                return BadRequest();
            }

            db.Entry(tariffs).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TariffsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tariffs
        [ResponseType(typeof(Tariffs))]
        public async Task<IHttpActionResult> PostTariffs(Tariffs tariffs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tariffs.Add(tariffs);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tariffs.TariffId }, tariffs);
        }

        // DELETE: api/Tariffs/5
        [ResponseType(typeof(Tariffs))]
        public async Task<IHttpActionResult> DeleteTariffs(int id)
        {
            Tariffs tariffs = await db.Tariffs.FindAsync(id);
            if (tariffs == null)
            {
                return NotFound();
            }

            db.Tariffs.Remove(tariffs);
            await db.SaveChangesAsync();

            return Ok(tariffs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TariffsExists(int id)
        {
            return db.Tariffs.Count(e => e.TariffId == id) > 0;
        }
    }
}