﻿namespace TariffTrade.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;
    using TariffTrade.Common.Models;
    using TariffTrade.Domain.Models;

    public class CodesController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Codes
        [ResponseType(typeof(Products))]
        public async Task<IHttpActionResult> GetCodes()
        {
            var a = await db.Codes.ToListAsync();
            var products = a.Select(p=>  new Products
            {
                Code = p.Code,
                Level = p.Level,
                Year = p.Year,
                Description = p.Descriptions.Select(d=>d.Description).LastOrDefault().ToString(),
                Hierarchy = p.Descriptions.Select(d => d.Hierarchy).LastOrDefault().ToString(),
                Country = p.Countries.Select(d => d.Country).LastOrDefault().ToString(),
                CovenantDate = Convert.ToDateTime(p.Countries.Select(d => d.CovenantDate).LastOrDefault()),
            });
           //var listProducts = new List<Products>();
           //listProducts.Add(products);
            return Ok(products.AsQueryable());
        }
        // GET: api/Codes/5
        [ResponseType(typeof(Codes))]
        public async Task<IHttpActionResult> GetCodes(int id)
        {
            Codes codes = await db.Codes.FindAsync(id);
            if (codes == null)
            {
                return NotFound();
            }

            return Ok(codes);
        }

        // PUT: api/Codes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCodes(int id, Codes codes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != codes.CodeId)
            {
                return BadRequest();
            }

            db.Entry(codes).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CodesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Codes
        [ResponseType(typeof(Codes))]
        public async Task<IHttpActionResult> PostCodes(Codes codes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Codes.Add(codes);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = codes.CodeId }, codes);
        }

        // DELETE: api/Codes/5
        [ResponseType(typeof(Codes))]
        public async Task<IHttpActionResult> DeleteCodes(int id)
        {
            Codes codes = await db.Codes.FindAsync(id);
            if (codes == null)
            {
                return NotFound();
            }

            db.Codes.Remove(codes);
            await db.SaveChangesAsync();

            return Ok(codes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CodesExists(int id)
        {
            return db.Codes.Count(e => e.CodeId == id) > 0;
        }
    }
}