﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TariffTrade.Common.Models;
using TariffTrade.Domain.Models;

namespace TariffTrade.API.Controllers
{
    public class DescriptionsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Descriptions
        public IQueryable<Descriptions> GetDescriptions()
        {
            return db.Descriptions;
        }

        // GET: api/Descriptions/5
        [ResponseType(typeof(Descriptions))]
        public async Task<IHttpActionResult> GetDescriptions(int id)
        {
            Descriptions descriptions = await db.Descriptions.FindAsync(id);
            if (descriptions == null)
            {
                return NotFound();
            }

            return Ok(descriptions);
        }

        // PUT: api/Descriptions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDescriptions(int id, Descriptions descriptions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != descriptions.DescriptionId)
            {
                return BadRequest();
            }

            db.Entry(descriptions).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DescriptionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Descriptions
        [ResponseType(typeof(Descriptions))]
        public async Task<IHttpActionResult> PostDescriptions(Descriptions descriptions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Descriptions.Add(descriptions);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = descriptions.DescriptionId }, descriptions);
        }

        // DELETE: api/Descriptions/5
        [ResponseType(typeof(Descriptions))]
        public async Task<IHttpActionResult> DeleteDescriptions(int id)
        {
            Descriptions descriptions = await db.Descriptions.FindAsync(id);
            if (descriptions == null)
            {
                return NotFound();
            }

            db.Descriptions.Remove(descriptions);
            await db.SaveChangesAsync();

            return Ok(descriptions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DescriptionsExists(int id)
        {
            return db.Descriptions.Count(e => e.DescriptionId == id) > 0;
        }
    }
}