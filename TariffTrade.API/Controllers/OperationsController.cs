﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TariffTrade.Common.Models;
using TariffTrade.Domain.Models;

namespace TariffTrade.API.Controllers
{
    public class OperationsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Operations
        public IQueryable<Operations> GetOperations()
        {
            return db.Operations;
        }

        // GET: api/Operations/5
        [ResponseType(typeof(Operations))]
        public async Task<IHttpActionResult> GetOperations(int id)
        {
            Operations operations = await db.Operations.FindAsync(id);
            if (operations == null)
            {
                return NotFound();
            }

            return Ok(operations);
        }

        // PUT: api/Operations/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOperations(int id, Operations operations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != operations.OperationId)
            {
                return BadRequest();
            }

            db.Entry(operations).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OperationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Operations
        [ResponseType(typeof(Operations))]
        public async Task<IHttpActionResult> PostOperations(Operations operations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Operations.Add(operations);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = operations.OperationId }, operations);
        }

        // DELETE: api/Operations/5
        [ResponseType(typeof(Operations))]
        public async Task<IHttpActionResult> DeleteOperations(int id)
        {
            Operations operations = await db.Operations.FindAsync(id);
            if (operations == null)
            {
                return NotFound();
            }

            db.Operations.Remove(operations);
            await db.SaveChangesAsync();

            return Ok(operations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OperationsExists(int id)
        {
            return db.Operations.Count(e => e.OperationId == id) > 0;
        }
    }
}