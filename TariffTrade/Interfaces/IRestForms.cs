﻿namespace TariffTrade.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using TariffTrade.Utils;

    public interface IRestForms
    {
        Task<T> Post<T>(
            string url, 
            Dictionary<string, object> simpleparams = null, 
            List<Param> complexparams = null);

        Task<T> Post<T, K>(string url, K objecttosend);

        Task<T> Get<T>(string url, Dictionary<string, object> formdata = null);

        Task<T> Delete<T>(string url, int id);
    }
}
