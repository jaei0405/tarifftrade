﻿using System;
namespace TariffTrade.Utils
{
    public class Constants
    {
        public enum Verbs
        {
            GET = 1,
            POST = 2
        }

        public static String UrlWebApi
        {
            get
            {
                return "https://tarifftradeapi.azurewebsites.net";
            }
        }

        public static string AppName = "Tariff Trade";

        public static string iOSClientId = "393194867877-objvftcdqat2b6im2j3gi55eho33m444.apps.googleusercontent.com";
        public static string AndroidClientId = "393194867877-so9v9vqio7s3805j97j0cmhuln5mfmck.apps.googleusercontent.com";
        public static string iOSSecretKey = "";
        public static string AndroidSecretKey = "";

        // These values do not need changing
        public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
        public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
        public static string AccessTokenUrl = "https://www.googleapis.com/oauth2/v4/token";
        public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";

        // Set these to reversed iOS/Android client ids, with :/oauth2redirect appended393194867877-so9v9vqio7s3805j97j0cmhuln5mfmck.apps.googleusercontent.com
        public static string iOSRedirectUrl = "googleusercontent.com.apps.393194867877-objvftcdqat2b6im2j3gi55eho33m444:/oauth2redirect";
        public static string AndroidRedirectUrl = "googleusercontent.com.apps.393194867877-so9v9vqio7s3805j97j0cmhuln5mfmck:/oauth2redirect";
    }
}
