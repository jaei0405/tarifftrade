﻿namespace TariffTrade.Models
{
    using Views;
    public class FacebookOAuthResult
    {
        public string AccessToken { get; set; }
        public LoginFacebookView FacebookPage { get; set; }
    }
}
