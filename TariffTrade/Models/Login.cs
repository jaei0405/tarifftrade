﻿using System;
namespace TariffTrade.Models
{
    public class Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Imagen { get; set; }
        public string Perfil { get; set; }
        public string Origen { get; set; }
        public Int64 Activo { get; set; }
        public Int64 Id { get; set; }
        public bool isAuthenticated { get; set; }

    }
}
