﻿namespace TariffTrade.ViewModels
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Common.Models;
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Services;
    using Utils;
    using Xamarin.Forms;

    public class OperationsViewModel : BaseViewModel
    {

        #region Attributes
        private string filter;
        private bool importIsVisible;
        private bool exportIsVisible;
        private bool isVisibleList;
        private bool isRefreshing;
        private bool isEnabled;
        private Countries countrySelected; 
        private ApiService apiServices;
        #endregion

        #region Properties
        public bool IsVisibleList
        {
            get { return this.isVisibleList; }
            set { this.SetValue(ref this.isVisibleList, value); }
        }

        public bool ImportIsVisible
        {
            get { return this.importIsVisible; }
            set { this.SetValue(ref this.importIsVisible, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { this.SetValue(ref this.isEnabled, value); }
        }

        public bool ExportIsVisible
        {
            get { return this.exportIsVisible; }
            set { this.SetValue(ref this.exportIsVisible, value); }
        }

        public ObservableRangeCollection<Countries> Countries
        {
            get;
            set;
        }

        public ObservableRangeCollection<Products> Products
        {
            get;
            set;
        }
        public ObservableRangeCollection<Products> AllProducts
        {
            get;
            set;
        }

        public string Filter
        {
            get { return this.filter; }
            set
            {
                this.filter = value;
                this.RefreshList();
            }
        }

        public Countries CountrySelected
        {
            get => countrySelected;
            set
            {
                if (this.SetProperty(ref countrySelected, value))
                    FilterItems();
            }
        }

        #endregion

        #region Constructors
        public OperationsViewModel()
        {
            Products = new ObservableRangeCollection<Products>();
            AllProducts = new ObservableRangeCollection<Products>();
            this.ImportIsVisible = false;
            this.ExportIsVisible = false;
            this.apiServices = new ApiService();
            this.LoadProducts();
            this.LoadCountries();
            ChangeCountryCommand = new Command(async () => await ChangeCountry());
        }

        #endregion

        #region Commands
        public ICommand ChangeCountryCommand
        {
            get;
            private set;
        }

        public ICommand ShowImportCommand
        {
            get
            {
                return new RelayCommand(ShowImport);
            }
        }
        public ICommand ShowExportCommand
        {
            get
            {
                return new RelayCommand(ShowExport);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(RefreshList);
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadProducts);
            }
        }
        #endregion

        private void RefreshList()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                IsVisibleList = false;
                Products.OrderBy(p => p.Description);
            }
            else
            {
                IsVisibleList = true;
                Products.ReplaceRange(AllProducts.Where(
                    a => a.Description.ToLower().Contains(this.Filter.ToLower()) || a.Code.ToLower().Contains(this.Filter.ToLower())).ToList()
                    .OrderBy(p => p.Description));
            }
        }

        public async Task ChangeCountry()
        {
            var products = this.Products;
            AllProducts.ReplaceRange(products);
            FilterItems();
        }

        public void FilterItems()
        {
            Products.ReplaceRange(AllProducts.Where(a => a.Country == CountrySelected.Country));
            this.IsEnabled = true;
        }

        private void ShowImport()
        {
            this.ImportIsVisible = true;
            this.ExportIsVisible = false;
        }

        private void ShowExport()
        {
            this.ImportIsVisible = false;
            this.ExportIsVisible = true;

        }

        private async void LoadCountries()
        {
            var connection = await this.apiServices.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        connection.Message,
                        "Accept");
                return;
            }

            var url = Constants.UrlWebApi;
            var prefix = "/api";
            var controller = "/Countries";
            var response = await this.apiServices.GetList<Countries>(
                url,
                prefix,
                controller
            );
            if (!response.IsSuccess) 
            {
                await Application.Current.MainPage.DisplayAlert(
                        "Error", 
                        response.Message, 
                        "Accept");
                return;
            }

            var list = (List<Countries>)response.Result;
            this.Countries = new ObservableRangeCollection<Countries>(list);
        }

        private async void LoadProducts() 
        {
            this.IsRefreshing = true;

            var connection = await this.apiServices.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        connection.Message,
                        "Accept");
                return;
            }

            var url = Constants.UrlWebApi;
            var prefix = "/api";
            var controller = "/Codes";
            var response = await this.apiServices.GetList<Products>(
                            url, 
                            prefix, 
                            controller);
            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                        "Error", 
                        response.Message, 
                        "Accept");
                return;
            }

            var list = (List<Products>)response.Result;
            this.AllProducts = new ObservableRangeCollection<Products>(list);
            //this.Products = new ObservableRangeCollection<Products>(list);
            this.IsRefreshing = false;
        }
    }
}
