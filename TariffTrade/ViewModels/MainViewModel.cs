﻿namespace TariffTrade.ViewModels
{
    public class MainViewModel
    {
        #region Properties
        public LoginViewModel Login { get; set; }
        public OperationsViewModel Operations { get; set; }
        #endregion
        public MainViewModel()
        {
            Login = new LoginViewModel();
            Operations = new OperationsViewModel();
        }
    }
}
