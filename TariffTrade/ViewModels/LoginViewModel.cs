﻿namespace TariffTrade.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Net.Http;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Newtonsoft.Json;
    using Services;
    using TariffTrade.Models;
    using TariffTrade.Utils;
    using TariffTrade.Views;
    using Xamarin.Auth;
    using Xamarin.Forms;

    public class LoginViewModel : INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Services
        DialogService dialogService;
        ApiService apiService;
        NavigationService navigationService;
        #endregion

        #region Attributes
        string _email;
        string _password;
        bool _isToggled;
        bool _isRunning;
        bool _isEnabled;
        bool _isVisible;
        Login _login = new Login();
        Account account = new Account();
        AccountStore store = AccountStore.Create();
        #endregion

        #region Properties
        public Login LoginObject
        {
            get { return _login; }
            set
            {
                if (_login != value)
                {
                    _login = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(LoginObject)));
                }
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (_isEnabled != value)
                {
                    _isEnabled = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsEnabled)));
                }
            }
        }

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                if (_isVisible != value)
                {
                    _isVisible = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsVisible)));
                }
            }
        }

        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
        }

        public bool IsToggled
        {
            get
            {
                return _isToggled;
            }
            set
            {
                if (_isToggled != value)
                {
                    _isToggled = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(IsToggled)));
                }
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(Password)));
                }
            }
        }
       
        public string Email
        {
            get 
            {
                return _email;
            }
            set 
            {
                if(_email != value) 
                {
                    _email = value;
                    PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(Email)));
                }
            }
        }
        #endregion

        #region Constructors
        public LoginViewModel()
        {
            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            IsEnabled = true;
            IsToggled = true;
        }
        #endregion

        #region Commands
        public ICommand RecoverPasswordCommand
        {
            get
            {
                return new RelayCommand(RecoverPassword);
            }
        }

        async void RecoverPassword()
        {
            //MainViewModel.GetInstance().PasswordRecovery =
            //    new PasswordRecoveryViewModel();
            //await navigationService.NavigateOnLogin("PasswordRecoveryView");
        }

        public ICommand LoginWithFacebookCommand
        {
            get
            {
                return new RelayCommand(LoginWithFacebook);
            }
        }

        async void LoginWithFacebook()
        {
            await Application.Current.MainPage.Navigation.PushAsync(
            new LoginFacebookView(
                "2003465046611319", 
                accesstokenresult: AccesTokenFacebook));
        }

        public ICommand RegisterNewUserCommand
        {
            get
            {
                return new RelayCommand(RegisterNewUser);
            }
        }

        async void RegisterNewUser()
        {
            //MainViewModel.GetInstance().NewCustomer = new NewCustomerViewModel();
            //await navigationService.NavigateOnLogin("NewCustomerView");
        }

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        async void Login()
        {
            if (string.IsNullOrEmpty(Email))
            {
                await dialogService.ShowMessage(
                    "Error",
                    "You must enter an email.");
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await dialogService.ShowMessage(
                    "Error",
                    "You must enter a password.");
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var connection = await apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage("Error", connection.Message);
                return;
            }

            var urlAPI = Constants.UrlWebApi;

            var response = await apiService.GetToken(
                urlAPI, 
                Email, 
                Password);

            if (response == null)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(
                    "Error", 
                    "The service is not available, please try latter.");
                Password = null;
                return;
            }

            if (string.IsNullOrEmpty(response.AccessToken))
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(
                    "Error", 
                    response.ErrorDescription);
                Password = null;
                return;
            }

            response.IsRemembered = IsToggled;
            response.Password = Password;

            Email = null;
            Password = null;

            IsRunning = false;
            IsEnabled = true;

            await Application.Current.MainPage.Navigation.PushAsync(
                    new OperationsView());
            
        }

        public ICommand LoginWithGoogleCommand
        {
            get
            {
                return new RelayCommand(LoginWithGoogle);
            }
        }

        async void LoginWithGoogle()
        {
            account = store.FindAccountsForService(Constants.AppName)
                        .FirstOrDefault();
            var authenticator = apiService.LoginWithGoogle();
            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;

            AuthenticationState.Authenticator = authenticator;

            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);
        }

        async void OnAuthCompleted(
            object sender, 
            AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            //User user = null;
            if (e.IsAuthenticated)
            {
                //If the user is authenticated, 
                //request their basic user data from Google
                //UserInfoUrl = https://www.googleapis.com/oauth2/v2/userinfo
                var request = new OAuth2Request(
                    "GET", 
                    new Uri(Constants.UserInfoUrl),
                    null, 
                    e.Account);
                var response = await request.GetResponseAsync();
                if (response != null)
                {
                    // Deserialize the data and store it in the account store
                    // The users email address will be 
                    // used to identify data in SimpleDB
                    string userJson = await response.GetResponseTextAsync();
                    //user = JsonConvert.DeserializeObject<User>(userJson);
                }

                if (account != null)
                {
                    store.Delete(account, Constants.AppName);
                }

                await store.SaveAsync(account = e.Account, Constants.AppName);
                await Application.Current.MainPage.Navigation.PushAsync(
                    new OperationsView());
               
            }
        }

        async void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }
           await dialogService.ShowMessage("Error", e.Message);
        }

        private async void AccesTokenFacebook(FacebookOAuthResult result)
        {
            //GraphApi graph = new GraphApi(result.AccessToken);
            await Application.Current.MainPage.Navigation.PushAsync(
                    new OperationsView());

        }
        #endregion
    }
}
